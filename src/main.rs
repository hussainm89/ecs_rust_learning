// Attempt at simple ECS. We do not store entities, but we make them out of components and iterate using systems
// COuld improve with far more effort using generics - Box<dyn .. >

// TODO:
// - Sound

use ecs_learning::render;
use ecs_learning::render::RenderDisplay;
use ecs_learning::render::DisplaySystemsActOn;
use std::thread::sleep;
use std::time::Duration;

fn main() {

    const TIME_STEP_MS: u64 = 1000/60;

    let mut display_entities: Vec<Box<dyn DisplaySystemsActOn>> = vec!(
        Box::new(
            RenderDisplay::new(30,40)
        ),
    );

    // intro
    render::write_string_system(&mut display_entities, "Welcome to rust ECS attempt".to_string());

    render::display_system(&display_entities);

    // take input for menu


    // then start game loop (interrupt with input requests)
    loop {
        
        sleep(Duration::from_millis(TIME_STEP_MS));
        
        render::display_system(&display_entities);
    }

}
