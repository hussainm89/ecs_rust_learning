// Make a component type using a trait

pub trait SystemActsOn {
    fn hello_from_component(&self);
    fn write_to_component(&mut self);
}

// Implement those components

pub struct TestComponent {
    x: i32,
    y: i32,
}

pub struct AnotherTestComponent {
    silly: bool,
}

impl AnotherTestComponent {
    pub fn new(silly: bool) -> Self {
        AnotherTestComponent {
            silly,
        }
    }
}

impl TestComponent {
    pub fn new(x: i32, y: i32) -> Self {
        TestComponent {
            x,
            y,
        }
    }
}

impl SystemActsOn for TestComponent {
    fn hello_from_component(&self) {
        println!("hello, from component.");
        println!("x: {0}, y: {1}", self.x, self.y);
    }
    fn write_to_component(&mut self) {
        self.x += 1;
    }
}

impl SystemActsOn for AnotherTestComponent {
    fn hello_from_component(&self) {
        println!("I am another component. SIlly status: {0}", self.silly);
    }
    fn write_to_component(&mut self) {
        self.silly = !self.silly;
    }
}

// define the system that acts on components implementing that trait

pub fn test_system(components: &Vec<Box<dyn SystemActsOn>>) {
    for comp in components {
        comp.hello_from_component();
    }
}

pub fn test_write_system(components: &mut Vec<Box<dyn SystemActsOn>>) {
    for comp in components {
        comp.write_to_component();
    }
}