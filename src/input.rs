
use std::io;

pub fn get_input(prompt: &str, max_choice: u8) -> u8 {
    loop {
        println!("{prompt}");
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        match input.trim().parse::<u8>() {
            Ok(num) if num > 0 && num <= max_choice => return num,
            _ => continue,
        }
    }
}