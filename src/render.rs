pub trait DisplaySystemsActOn {
    fn display(&self) {

    }
    fn write_to(&mut self, _coord_row: usize, _coord_col: usize, _new_char: char) {

    }
    fn get_num_rows_cols(&self) -> (usize, usize) {
        (0,0)
    }
}

pub struct RenderDisplay {
    // rows: u32,
    // cols: u32,
    // cols: Vec<char>,
    all: Vec<Vec<char>>,
}

impl RenderDisplay {
    pub fn new(rows: usize, cols: usize) -> Self {
        let mut new_render_display = RenderDisplay {
            // cols: Vec<char>::new(),
            all: Vec::<Vec<char>>::new(),
        };
        for _ in 0..rows {
            let mut new_col = Vec::<char>::new();
            for _ in 0..cols {
                new_col.push(' ');
            }
            new_render_display.all.push(new_col);
            // let col: Vec<char> = Vec<char>::new();
        }
        new_render_display
    }
}

impl DisplaySystemsActOn for RenderDisplay {
    fn display(&self) {
        match std::process::Command::new("cls").status() {
            Ok(_) => (),
            Err(_) => match std::process::Command::new("clear").status() {
                Ok(_) => (),
                Err(_) => println!("Error clearing screen"),
            }
        }
        for col in &self.all {
            for c in col {
                print!("{0} ", c.to_string());
            }
            println!("");
        }
    }

    fn write_to(&mut self, coord_row: usize, coord_col: usize, new_char: char) {
        // let row = self.all.get(coord_row);
        if coord_row > self.all.len() - 1 {
            println!("error writing: out of bounds");
            return;
        }
        if coord_col > self.all[coord_row].len() - 1 {
            println!("error writing: col out of bounds");
            return;
        }
        self.all[coord_row][coord_col] = new_char;
        // println!("{}", self.all[coord_row][coord_col].to_string());
    }
    fn get_num_rows_cols(&self) -> (usize, usize) {
        (self.all.len(), self.all[0].len())
    }
}

pub fn display_system(components: &Vec<Box<dyn DisplaySystemsActOn>>) {
    for comp in components {
        comp.display();
    }
}

// fn write_to_display_system(components: &mut Vec<Box<dyn DisplaySystemsActOn>>,
//         coord_row: usize, coord_col: usize, new_char: char) {
//     for comp in components {
//         comp.write_to(coord_row,coord_col, new_char);
//     }
// }

fn write_string_to_display_system(components: &mut Vec<Box<dyn DisplaySystemsActOn>>, coord_row: usize,
        coord_col: usize, string: &str) {
        let char_arr: Vec<char> = string.chars().collect();
        for comp in components {
            for i in 0..char_arr.len(){
                comp.write_to(coord_row, coord_col + i, char_arr[i]);
            }
        }
}

pub fn write_string_system(components: &mut Vec<Box<dyn DisplaySystemsActOn>>, string: String) {
    if components.len() == 0 {
        return;
    }    
    
    let max_line_len = components[0].get_num_rows_cols().1;
    let max_v_len = components[0].get_num_rows_cols().0;
    let words: Vec<&str> = string.split(' ').collect();
    let mut result: Vec<String> = Vec::new();
    let mut chunk = String::new();
    for i in 0..words.len() {
        if chunk.is_empty() {
            chunk.push_str(words[i]);
        }
        else if chunk.len() + words[i].len() <= max_line_len {
            chunk.push(' ');
            chunk.push_str(words[i]);
        }
        else {
            result.push(chunk);
            chunk = String::from(words[i]);
        }
    }
    if !chunk.is_empty() {
        result.push(chunk);
    }

    let mut row_to_write_in = if result.len() < max_v_len { (max_v_len - result.len())/2 } else { 0 };
    // println!("{}",row_to_write_in);
    for word in &result {
        write_string_to_display_system(components, 
            row_to_write_in,
            if word.len() < max_line_len { (max_line_len - word.len()) / 2 } else { 0 },
            word);
            row_to_write_in += 1;
    }

}